#![cfg_attr(nightly, feature(step_trait))]
#![recursion_limit = "2048"]
use num_traits::identities::{one, zero, One, Zero};

// included my original stab at an interator-based implementation, but it's less elegant given some trait constraints
#[cfg(nightly)]
mod iteratord;

use std::ops::Sub;

/// Recursive implementation of multiplication.
///
/// Pros:
/// * works in stable
/// * doesn't rely on new traits
///
/// Cons:
/// * can (will) overflow the stack because we don't have tail call optimization in this language
///
/// (see multiply-hs for this implementation without the downsides)
pub fn multiply<T>(lhs: T, rhs: T) -> T
where
    T: Copy + PartialOrd + PartialEq + Zero + One + Sub<Output = T>,
{
    match (rhs == zero(), rhs < zero()) {
        (true, _) => zero(),
        (_, true) => zero::<T>() - multiply(lhs, zero::<T>() - rhs),
        _ => lhs + multiply(lhs, rhs - one()),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::{prop_assert, prop_assert_eq, proptest};

    proptest! {
        #[test]
        fn test_multiply_u8(lhs: u8, rhs: u8) {
                        let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_u16(lhs: u16, rhs in 0_u16..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_u32(lhs: u32, rhs in 0_u32..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_u64(lhs: u64, rhs in 0_u64..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_usize(lhs: usize, rhs in 0_usize..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i8(lhs: i8, rhs: i8) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i16(lhs: i16, rhs in 0_i16..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i32(lhs: i32, rhs in 0_i32..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i64(lhs: i64, rhs in 0_i64..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_isize(lhs: isize, rhs in 0_isize..2048) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }
    }
}
