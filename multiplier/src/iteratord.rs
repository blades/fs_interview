use num_traits::identities::{one, zero, One, Zero};
use std::iter::Step;
use std::ops::Add;

/// The basic algorithm is easy, and hopefully elegant.
///
/// The devil is in the details.
/// * The Step trait is nightly-only, which means that this function only works as generic if we build in nightly.
/// * Abs is not implemented as a trait, but it's only available on the primitive types. I made my own trait as a workaround.
#[cfg(nightly)]
pub fn multiply<T: TryAbs + Add + Zero + Step + Copy + Sub<Output = T>>(lhs: T, rhs: T) -> T {
    match rhs < zero() {
        true => (zero()..rhs.try_abs()).fold(zero(), |acc, _| acc - lhs),
        false => (zero()..rhs.try_abs()).fold(zero(), |acc, _| acc + lhs),
    }
}

/// Absolute value
pub trait TryAbs {
    fn try_abs(self) -> Self;
}

macro_rules! abs_unsigned {
    ( $x:ty ) => {
        impl TryAbs for $x {
            fn try_abs(self) -> Self {
                self
            }
        }
    };
}

macro_rules! abs_signed {
    ( $x:ty ) => {
        impl TryAbs for $x {
            fn try_abs(self) -> Self {
                self.abs()
            }
        }
    };
}

// implement TryAbs for the integer primitives
abs_unsigned!(usize);
abs_unsigned!(u8);
abs_unsigned!(u16);
abs_unsigned!(u32);
abs_unsigned!(u64);
abs_signed!(isize);
abs_signed!(i8);
abs_signed!(i16);
abs_signed!(i32);
abs_signed!(i64);

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::{prop_assert, prop_assert_eq, proptest};

    proptest! {
        #[test]
        fn test_multiply_u8(lhs: u8, rhs: u8) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }


        #[test]
        fn test_multiply_u16(lhs in 0_u16..1000, rhs in 0_u16..1000) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_u32(lhs: u32, rhs: u32) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_u64(lhs: u64, rhs: u64) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_usize(lhs: usize, rhs: usize) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i8(lhs: i8, rhs: i8) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i16(lhs: i16, rhs: i16) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i32(lhs: i32, rhs: i32) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_i64(lhs: i64, rhs: i64) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }

        #[test]
        fn test_multiply_isize(lhs: isize, rhs: isize) {
            let expected = std::panic::catch_unwind(|| lhs * rhs);
            match expected {
                Err(_) => {
                    let actual = std::panic::catch_unwind(|| multiply(lhs, rhs));
                    prop_assert!(actual.is_err());
                },
                Ok(expected) => {
                    let actual = multiply(lhs, rhs);
                    prop_assert_eq!(expected, actual);
                }
            }
        }
    }
}
