## Multiply without mul or div

I interpreted this challenge to mean a generic algorithm across all integer types.

I encountered some gaps in the built-in Rust traits for this, so I have included 3 implementations here.

### Haskell
`./multiply-hs/multiplier.hs`

I wanted to implement everything in Rust, but given the challenges with Rust, I wanted to show my intended solution succinctly.

### Rust stable
`./multiplier/src/lib.rs`

This solution is succinct and hopefully elegant. The problem here is that the recursion easily blows the stack.

`pushd multiplier; cargo test`

### Rust nightly
`./multiplier/src/iteratord.rs`

This solution uses an iterator to avoid the issue with recursion. This requires nightly because we can't range over generic types because the `Step` trait is nightly-only.

`pushd multiplier; cargo +nightly test`

## Hashmap
`./hashed-mapped/src/lib.rs`

I wanted a hashmap with a minimal but useful api.

Here I implemented a generic `Table` which can accept any hashing algorithm (given it implements the correct trait).

`Table` handles collisions via chaining, and dynamically resizes.

`BladesHashMap` is based on `Table` using `FarmHash`.
