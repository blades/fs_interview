multiply' :: Int -> Int -> Int
multiply' _ 0 = 0
multiply' a b
  | b < 0 = 0 - (multiply' a (0-b))
  | otherwise = a + (multiply' a (b-1))

prop_Multiply a b = a * b == multiply' a b
