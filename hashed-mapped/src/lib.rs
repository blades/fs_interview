use farmhash::FarmHasher;
use std::collections::LinkedList;
use std::hash::{BuildHasher, Hash, Hasher};

static LOAD_FACTOR: f32 = 0.9;
static INITIAL_CAPACITY: usize = 300;

struct FarmHashBuilder {}

impl BuildHasher for FarmHashBuilder {
    type Hasher = FarmHasher;

    fn build_hasher(&self) -> Self::Hasher {
        Self::Hasher::default()
    }
}

/// BladesHashMap is based on FarmHash.
///
/// Collision resolution is handled via bucket-chaining with a linked list.
pub struct BladesHashMap<K, V>
where
    K: PartialEq + Hash,
{
    inner: Table<K, V, FarmHashBuilder>,
}

impl<K, V> Default for BladesHashMap<K, V>
where
    K: PartialEq + Hash,
{
    fn default() -> Self {
        BladesHashMap {
            inner: Table::with_capacity(INITIAL_CAPACITY, FarmHashBuilder {}),
        }
    }
}

impl<K, V> BladesHashMap<K, V>
where
    K: PartialEq + Hash,
{
    /// Create a new hashmap
    pub fn new() -> Self {
        Self::default()
    }

    /// Lookup an item in the hashmap.
    pub fn get(&self, key: &K) -> Option<&V> {
        self.inner.get(key)
    }

    /// Insert or overwrite an item in the hashmap. If the item already existed, its previous value is returned.
    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        self.inner.insert(key, value)
    }

    /// Remove an item from the hashmap. If the item exists, its value will be returned.
    pub fn remove(&mut self, key: &K) -> Option<V> {
        self.inner.remove(key)
    }

    /// How many items are currently stored in this hashmap
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    /// Is the hashmap is empty?
    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }

    /// Total capacity of this hashmap
    pub fn capacity(&self) -> usize {
        self.inner.capacity()
    }
}

/// Table uses a LinkedList for collision resolution.
///
/// advantages:
/// * straightforward
/// * memory efficient in the common case
///
/// disadvantages:
/// * slow
///
/// ## dynamic resizing
///
/// When the table hits the max load factor, the table grows by 2x.
///
/// The algorithm here is very simple, a new bucket vec is created and items are re-hashed and moved to the new vec.
struct Table<K, V, H>
where
    K: PartialEq + Hash,
    H: BuildHasher,
{
    items: usize,
    capacity: usize,
    hasher: H,
    buckets: Vec<LinkedList<(K, V)>>,
}

impl<K, V, H> Table<K, V, H>
where
    K: PartialEq + Hash,
    H: BuildHasher,
{
    /// Create a hashmap of a given starting size.
    pub fn with_capacity(capacity: usize, hasher: H) -> Self {
        let mut buckets = Vec::with_capacity(capacity);
        for _ in 0..capacity {
            buckets.push(LinkedList::new())
        }
        Table {
            items: 0,
            capacity,
            hasher,
            buckets,
        }
    }

    /// Lookup an item in the hashmap.
    pub fn get(&self, key: &K) -> Option<&V> {
        let idx = self.hash_to_bucket_idx(key);
        for (k, v) in self.buckets[idx].iter() {
            if k == key {
                return Some(v);
            }
        }
        None
    }

    /// Insert or overwrite an item in the hashmap. If the item already existed, its previous value is returned.
    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        let idx = self.hash_to_bucket_idx(&key);
        if self.items as f32 / self.capacity as f32 > LOAD_FACTOR {
            self.resize();
        }

        for (k, v) in self.buckets[idx].iter_mut() {
            if k == &key {
                return Some(std::mem::replace(v, value));
            }
        }

        self.items += 1;
        self.buckets[idx].push_front((key, value));

        None
    }

    /// Remove an item from the hashmap. If the item exists, its value will be returned.
    pub fn remove(&mut self, key: &K) -> Option<V> {
        let idx = self.hash_to_bucket_idx(&key);

        let mut new_list = LinkedList::new();
        let mut ret = None;
        while let Some((k, v)) = self.buckets[idx].pop_front() {
            if &k == key {
                ret = Some(v);
                self.items -= 1;
                continue;
            }
            new_list.push_back((k, v));
        }
        ret
    }

    /// How many items are currently stored in this hashmap
    pub fn len(&self) -> usize {
        self.items
    }

    /// Is the hashmap is empty?
    pub fn is_empty(&self) -> bool {
        self.items == 0
    }

    /// Total capacity of this hashmap
    pub fn capacity(&self) -> usize {
        self.capacity
    }

    fn hash_to_bucket_idx(&self, key: &K) -> usize {
        let mut h = self.hasher.build_hasher();
        key.hash(&mut h);
        let hash = h.finish() as usize;

        hash % self.capacity
    }

    // grow hashmap by 2x
    fn resize(&mut self) {
        let new_cap = match self.capacity {
            0 => INITIAL_CAPACITY,
            x => x * 2,
        };

        let mut new_buckets = Vec::with_capacity(new_cap);
        for _ in 0..new_cap {
            new_buckets.push(LinkedList::new());
        }

        let mut old_buckets = std::mem::replace(&mut self.buckets, new_buckets);
        self.capacity = new_cap;
        self.items = 0;

        for mut bucket in old_buckets.drain(0..) {
            while let Some((k, v)) = bucket.pop_front() {
                self.insert(k, v);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hashmap() {
        let mut hm = BladesHashMap::new();

        // insert
        hm.insert("foo", "bar");
        assert_eq!(hm.get(&"foo").unwrap(), &"bar");
        assert!(hm.get(&"bar").is_none());

        // remove
        assert_eq!(hm.remove(&"foo").unwrap(), "bar");
        assert!(hm.get(&"foo").is_none());
    }

    #[test]
    fn test_resize() {
        let mut fm = BladesHashMap::new();
        assert_eq!(INITIAL_CAPACITY, fm.capacity());

        for x in 0..=300 {
            assert!(fm.insert(x, x * 7).is_none());
        }

        assert_eq!(INITIAL_CAPACITY * 2, fm.capacity());

        for x in 0..=300 {
            println!("{}", x);
            assert!(fm.get(&x).is_some());
            assert_eq!(&(x * 7), fm.get(&x).unwrap());
        }
    }
}
